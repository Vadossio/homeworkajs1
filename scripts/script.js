"use strict";
// 1. Коли ми хочемо прочитати властивість з Об'єкту, а вона відсутня, JavaScript автоматично бере його з прототипу.
// 2. Щоб викликати батьківський конструктор.



class Employee {
    constructor (name, age, salary) {
    this.name = name;
    this.age = age;
    this.salary = salary;

    }
    set name(nam) {
        this._name = nam;
    }
    get name() {
        return this._name;
    }

    set age(val) {
        this._age = val;
    }
    get age() {
        return this._age;
    }
    set salary(sal) {
        this._salary = sal;
    }
    get salary() {
        return this._salary;
    }
    
}

const employ = new Employee (
    "Vadim",
    "26",
    "10000",
)
console.log(employ);

class Programmer extends Employee {
    constructor (name, age, salary, lang) {
        super(name,age,salary);
        this.lang = lang;
    }
    set salary(sal) {
        this._salary = sal * 3;
    }
    get salary() {
        return this._salary;
    }
    set lang(language) {
        this._lang = language;
    }
    get lang() {
        return this._lang; 
    }
}

const prog = new Programmer (
    "Vadim",
    "26",
    "10000",
    "Ukrainian",
)
const prog2 = new Programmer (
    "Vadim",
    "26",
    "10000",
    "Ukrainian",
)
const prog3 = new Programmer (
    "Vadim",
    "26",
    "10000",
    "Ukrainian",
)
console.log(prog);
console.log(prog2);
console.log(prog3);